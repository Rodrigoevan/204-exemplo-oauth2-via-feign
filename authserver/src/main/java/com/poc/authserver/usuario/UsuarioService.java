package com.poc.authserver.usuario;

import java.util.Collections;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService {
  @Autowired
  private BCryptPasswordEncoder encoder;
  
  @Autowired
  private UsuarioRepository repository;
  
  @PostConstruct
  public void popular() {
    Usuario usuario = new Usuario();
    usuario.setId(1);
    usuario.setNome("jose");
    usuario.setSenha(encoder.encode("jose123"));
    
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(2);
    usuario.setNome("leandro");
    usuario.setSenha(encoder.encode("leandro123"));
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(3);
    usuario.setNome("kaique");
    usuario.setSenha(encoder.encode("kaique123"));
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(4);
    usuario.setNome("pedro");
    usuario.setSenha(encoder.encode("pedro123"));
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(5);
    usuario.setNome("luis");
    usuario.setSenha(encoder.encode("luis123"));
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(6);
    usuario.setNome("chrislucas");
    usuario.setSenha(encoder.encode("chrislucas123"));
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(7);
    usuario.setNome("vittoria");
    usuario.setSenha(encoder.encode("vittoria123"));
    repository.save(usuario);

    usuario = new Usuario();
    usuario.setId(8);
    usuario.setNome("fellipe");
    usuario.setSenha(encoder.encode("fellipe123"));
    repository.save(usuario);

  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<Usuario> optional = repository.findByNome(username);
    
    if(!optional.isPresent()) {
      throw new UsernameNotFoundException("Usuário não encontrado");
    }
    
    Usuario usuario = optional.get();
    
    SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");
    
    return new User(usuario.getNome(), usuario.getSenha(), Collections.singletonList(authority));
  }
}
